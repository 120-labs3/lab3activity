import java.util.Scanner;

public class Application{
	
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		Student student1 = new Student();
		
		System.out.println("Enter the Student's name");
		student1.name = input.next();
		System.out.println("Enter the Student's ID");
		student1.studentID = input.next();
		System.out.println("Enter the corresponding Grade: ");
		student1.grade = input.nextInt();
		
		System.out.println("The student is called: " + student1.name + "Their Id is: " + student1.studentID + "And their grade is: " + student1.grade);
		
		Student student2 = new Student();
		
		
		System.out.println("Enter the Student's name");
		student2.name = input.next();
		System.out.println("Enter the Student's ID");
		student2.studentID = input.next();
		System.out.println("Enter the corresponding Grade: ");
		student2.grade = input.nextInt();
		
		System.out.println("The student is called: " + student2.name + "Their Id is: " + student2.studentID + "And their grade is: " + student2.grade);
		
		student1.sayHi();
		student2.sayHi(); 
		
		Student[] section3 = new Student[3];
		
		//For Testing purposes
		//student1.name = "vj";
		//student2.name = "maj";
		
		section3[0] = student1;
		section3[1] = student2;
		section3[2] = new Student();
		
		section3[2].name = "Jamie";
		
		System.out.println(section3[0].name);
		
		System.out.println(section3[2].name);
		
	}
}